'''
Integrantes:
Mateo Cunha
Eduardo Palmiero
Jonatan Ravinovitch
Juan Ignacio Sabino
Tomás Guzman
Adrián Rubado
'''
import ciw
import random

'''
El tiempo entre llegadas de los clientes a la pelu es distr. exponencial con media de 15 minutos
'''
media_tasa_de_arribo = 15
lambda_tasa_de_arribo = 1 / media_tasa_de_arribo
'''
Cantidad de servidores(peluqueros)
'''
s_cantidad_de_servidores = 1
'''
Tiempos del servicio en minutos, entre 10 y 15.
'''
tiempo_minimo = 10
tiempo_maximo = 15
'''
Cantidad de tiempo que queremos simular. 480 es la cantidad de minutos de un dia laboral de 8hs.
La cantidad de simulaciones es cuantas veces queremos repetir el experimento completo
Dejamos estas variables aca en caso de que queramos variar los valores.
'''
dias = 1
minutos_por_dia = 480
cantidad_de_simulaciones = 100

# Aca definimos como va a ser nuestro sistema.
network = ciw.create_network(
    Arrival_distributions=[['Exponential', lambda_tasa_de_arribo]],
    Service_distributions=[['Uniform', tiempo_minimo, tiempo_maximo]],
    Number_of_servers=[s_cantidad_de_servidores]
)

tiempo_promedio_de_servicio = []
tiempo_promedio_de_espera = []
tamanio_promedio_de_cola = []
for i in range(cantidad_de_simulaciones):
    '''
    Lo primero que hacemos es setear un numero aleatorio de seed para que el experimento use siempre valores distintos.
    Despues creamos una cola que simularemos con nuestro sistema o network
    El tercer metodo corre la simulacion la cantidad de minutos que hayamos seteado
    Y despues nos traemos los registros de esa simulacion.
    
    Despues lo que hacemos es meter en cada una de nuestras listas los promedios de las columnas que hemos extraido.
    Extraemos 'service_time' para calcular el tiempo promedio de servicio
    Extraemos 'waiting_time' para calcular cuanto se espera en promedio hasta que sos atendido
    Extraemos 'queue_size_at_arrival' para calcular el promedio de tamanio de la cola cuando llegamos a la peluqueria.
    '''
    ciw.seed(random.randint(0, 1000))
    cola = ciw.Simulation(network)
    cola.simulate_until_max_time(dias * cantidad_de_simulaciones)
    recs = cola.get_all_records()

    tiempos_servicio = [r.service_time for r in recs]
    tiempos_espera = [r.waiting_time for r in recs]
    tamanio_cola = [r.queue_size_at_arrival for r in recs]

    tiempo_promedio_de_servicio.append(sum(tiempos_servicio) / len(tiempos_servicio))
    tiempo_promedio_de_espera.append(sum(tiempos_espera) / len(tiempos_espera))
    tamanio_promedio_de_cola.append(sum(tamanio_cola) / len(tamanio_cola))

utilizacion_promedio_del_servicio = sum(tiempo_promedio_de_servicio) / len(tiempo_promedio_de_servicio)
longitud_promedio_de_la_cola = sum(tamanio_promedio_de_cola) / len(tamanio_promedio_de_cola)
tiempo_promedio_que_un_cliente_espera = sum(tiempo_promedio_de_espera) / len(tiempo_promedio_de_espera)

print('La utilizacion promedio del servicio es de ' + str("{0:.1f}".format(utilizacion_promedio_del_servicio)) + ' minutos.')
print('La cantidad promedio de clientes que esperan es de ' + str("{0:.1f}".format(longitud_promedio_de_la_cola)) + ' personas.')
print('El tiempo promedio que un cliente espera en la cola es de ' + str("{0:.1f}".format(tiempo_promedio_que_un_cliente_espera)) + ' minutos.')
